﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeiseNädalaTeisipäevMuusika
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Beep(440, 500);
            Console.Beep(880, 500);
            int nootDo = (int)(440.0 * Math.Pow(2, 1.0 / 4));
            Console.WriteLine(nootDo);
            Console.Beep(nootDo, 1000);

            Enumerable.Range(0, 12)
                .Select(x => (int)(300 * Math.Pow(2, x / 12.0)))
                .ToList()
                .ForEach(x => Console.Beep(x, 100));
            // suht suvaline sai tehtud, sest kui ei ole seda kuulmist siis ei ole...
        }
    }
}
